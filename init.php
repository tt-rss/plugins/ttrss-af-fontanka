<?php

class Af_Fontanka extends Plugin {

	function about() {
		return array(1.0,
			"Gets full text content from fontanka.ru RSS feed",
			"fox");
	}

	function init($host) {
		$host->add_hook($host::HOOK_ARTICLE_FILTER, $this);
	}

	function hook_article_filter($article) {
		if (strpos($article["link"], ".fontanka.ru") !== FALSE) {
			$tmp = fetch_file_contents(["url" => $article["link"]]);

			if ($tmp) {
				$doc = new DOMDocument("1.0", "UTF-8");

				if (!@$doc->loadHTML($tmp))
					return $article;

				$xpath = new DOMXPath($doc);
				$base_node = $xpath->query("//article")->item(0);

				if ($base_node) {
					$article["content"] = $doc->saveHTML($base_node);
				}
			}
		}

		return $article;
	}

	function api_version() {
		return 2;
	}

}
